/*
 * Copyright (c) 2020 by Hotspring Ventures Limited
 * 1 Regent Street (c/o Calder & Co), London SW1Y 4NW
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of Hotspring Ventures Limited ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement
 * you entered into with Hotspring Ventures Limited.
 */

package org.example;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.example.commands.DomainCommand;
import org.example.events.DomainEvent;
import org.example.queries.DomainQuery;
import org.junit.jupiter.api.BeforeEach;

public class BaseTest {

    private List<DomainEvent> history;
    private List<DomainEvent> publishedEvents;
    private Object queriedResponse;
    private FinalReservations finalReservations;

    @BeforeEach
    public void Setup() {
        history = new ArrayList<>();
        publishedEvents = new ArrayList<>();
        queriedResponse = null;
    }

    protected void given(DomainEvent... events) {
        history = List.of(events);
        finalReservations = new FinalReservations(history);
    }

    protected void when(DomainCommand command) {

        EventPublisher eventPublisher = event -> {
            publishedEvents.add(event);
        };
        new CommandHandler(history, eventPublisher)
            .handle(command);
    }

    protected void whenQuery(DomainQuery query) {
        ResponseConsumer  consumer = o -> {
            queriedResponse = o;
        };
        QueryHandler handler = new QueryHandler(finalReservations, consumer);
        handler.query(query);
    }

    protected void thenExpect(DomainEvent... expectedEvents) {
        assertThat(publishedEvents).containsExactly(expectedEvents);
    }

    protected void thenExpectResponse(Object expectedResponse) {
        assertThat(queriedResponse).isEqualTo(expectedResponse);
    }
}
