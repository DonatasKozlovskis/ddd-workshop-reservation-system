package org.example;

import java.util.Collections;
import java.util.List;

import org.example.events.SeatHasBeenReserved;
import org.example.queries.ReservedSeatsQuery;
import org.junit.jupiter.api.Test;

public class QueryHandlerTest extends BaseTest {

    @Test
    public void shouldGetReservedSeats() {
        given(
            new SeatHasBeenReserved(new Seat("Seat1")),
            new SeatHasBeenReserved(new Seat("Seat2")),
            new SeatHasBeenReserved(new Seat("Seat3"))
        );
        whenQuery(new ReservedSeatsQuery());

        thenExpectResponse(
            new ReservedSeatsResponse(
                List.of(
                    new Seat("Seat1"),
                    new Seat("Seat2"),
                    new Seat("Seat3")
                )
            )
        );
    }

    @Test
    public void shouldNotGetAnyReservedSeats() {
        given(
            // no reserved seats
        );
        whenQuery(new ReservedSeatsQuery());

        thenExpectResponse(
            new ReservedSeatsResponse(Collections.emptyList())
        );
    }
}
