package org.example;

import org.example.commands.ReserveSeatCommand;
import org.example.events.ReservationsCutOff;
import org.example.events.SeatHasBeenReserved;
import org.example.events.SeatReservationDeclined;
import org.junit.jupiter.api.Test;

public class CommandHandlerTest extends BaseTest {

    @Test
    public void shouldReserveASeatWhichIsNotAlreadyReserved() {
        given(
            //nothing has been reserved before
        );
        when(
            new ReserveSeatCommand("seat1")
        );
        thenExpect(
            new SeatHasBeenReserved(new Seat("seat1"))
        );
    }

    @Test
    public void shouldNotReserveASeatWhichIsAlreadyReserved() {
        given(
            new SeatHasBeenReserved(new Seat("seat1"))
        );
        when(
            new ReserveSeatCommand("seat1")
        );
        thenExpect(
            new SeatReservationDeclined(new Seat("seat1"))
        );
    }

    @Test
    public void shouldNotReserveASeatWhenReservationCutOff() {
        given(
            new ReservationsCutOff()
        );
        when(
            new ReserveSeatCommand("seat1")
        );
        thenExpect(
            new SeatReservationDeclined(new Seat("seat1"))
        );
    }

}
