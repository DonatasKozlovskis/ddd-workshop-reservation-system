/*
 * Copyright (c) 2020 by Hotspring Ventures Limited
 * 1 Regent Street (c/o Calder & Co), London SW1Y 4NW
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of Hotspring Ventures Limited ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement
 * you entered into with Hotspring Ventures Limited.
 */

package org.example;

import java.util.List;

import org.example.commands.DomainCommand;
import org.example.commands.ReserveSeatCommand;
import org.example.events.DomainEvent;

public class CommandHandler {

    private final List<DomainEvent> pastEvents;
    private final EventPublisher eventPublisher;

    public CommandHandler(List<DomainEvent> pastEvents, EventPublisher eventPublisher) {
        // events repository
        this.pastEvents = pastEvents;
        this.eventPublisher = eventPublisher;
    }

    public void handle(DomainCommand command) {
        if (command instanceof ReserveSeatCommand) {

            ScreeningReservationsState state = new ScreeningReservationsState(pastEvents);
            ScreeningReservations reservations = new ScreeningReservations(eventPublisher, state);

            Seat seat = new Seat(((ReserveSeatCommand) command).getSeat());
            reservations.reserveSeat(seat);
        }
    }
}
