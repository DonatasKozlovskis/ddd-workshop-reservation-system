/*
 * Copyright (c) 2020 by Hotspring Ventures Limited
 * 1 Regent Street (c/o Calder & Co), London SW1Y 4NW
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of Hotspring Ventures Limited ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement
 * you entered into with Hotspring Ventures Limited.
 */

package org.example;

import java.util.function.Consumer;

import org.example.queries.DomainQuery;
import org.example.queries.ReservedSeatsQuery;

public class QueryHandler {

    private final FinalReservations reservations;
    private final ResponseConsumer responseConsumer;

    public QueryHandler(FinalReservations reservations,
                        ResponseConsumer responseConsumer) {
        this.reservations = reservations;
        this.responseConsumer = responseConsumer;
    }

    public void query(DomainQuery query) {
        if (query instanceof ReservedSeatsQuery) {
            ReservedSeatsResponse response = new ReservedSeatsResponse(reservations.getReservedSeats());
            responseConsumer.consume(response);
        }
    }
}
