/*
 * Copyright (c) 2020 by Hotspring Ventures Limited
 * 1 Regent Street (c/o Calder & Co), London SW1Y 4NW
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of Hotspring Ventures Limited ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement
 * you entered into with Hotspring Ventures Limited.
 */

package org.example;

import java.util.ArrayList;
import java.util.List;

import org.example.events.DomainEvent;
import org.example.events.ReservationsCutOff;
import org.example.events.SeatHasBeenReserved;

public class ScreeningReservationsState {

    private final List<Seat> reservedSeats;
    private Boolean reservationIsOpen;

    public ScreeningReservationsState(
        List<DomainEvent> pastEvents
    ) {
        this.reservedSeats = new ArrayList<>();
        this.reservationIsOpen = true;
        restoreState(pastEvents);
    }

    public List<Seat> getReservedSeats() {
        return reservedSeats;
    }

    public Boolean isReservationOpen() {
        return reservationIsOpen;
    }

    private void restoreState(List<DomainEvent> pastEvents) {
        pastEvents.forEach(domainEvent -> {
            if (domainEvent instanceof SeatHasBeenReserved) {
                reservedSeats.add(((SeatHasBeenReserved) domainEvent).getReservedSeat());
            }
            if (domainEvent instanceof ReservationsCutOff) {
                reservationIsOpen = false;
            }
        });
    }
}
