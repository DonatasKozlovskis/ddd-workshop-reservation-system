/*
 * Copyright (c) 2020 by Hotspring Ventures Limited
 * 1 Regent Street (c/o Calder & Co), London SW1Y 4NW
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of Hotspring Ventures Limited ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement
 * you entered into with Hotspring Ventures Limited.
 */

package org.example;

import org.example.events.SeatHasBeenReserved;
import org.example.events.SeatReservationDeclined;

public class ScreeningReservations {

    private final ScreeningReservationsState state;
    private final EventPublisher publisher;

    public ScreeningReservations(
        EventPublisher publisher,
        ScreeningReservationsState state
    ) {
        this.publisher = publisher;
        this.state = state;
    }

    public void reserveSeat(Seat seat) {
        if (state.isReservationOpen() && seatIsAvailable(seat)) {
            publisher.publish(new SeatHasBeenReserved(seat));
        } else {
            publisher.publish(new SeatReservationDeclined(seat));
        }
    }

    private boolean seatIsAvailable(Seat seat) {
        return !state.getReservedSeats().contains(seat);
    }
}
