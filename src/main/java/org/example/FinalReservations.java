/*
 * Copyright (c) 2020 by Hotspring Ventures Limited
 * 1 Regent Street (c/o Calder & Co), London SW1Y 4NW
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of Hotspring Ventures Limited ("Confidential Information").
 * You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement
 * you entered into with Hotspring Ventures Limited.
 */

package org.example;

import java.util.ArrayList;
import java.util.List;

import org.example.events.DomainEvent;
import org.example.events.SeatHasBeenReserved;
import org.example.events.SeatsWerePreparedForReservation;

public class FinalReservations {

    private final List<Seat> reservedSeats;
    private final List<Seat> availableSeats;

    public FinalReservations(List<DomainEvent> events) {
        this.reservedSeats = new ArrayList<>();
        this.availableSeats = new ArrayList<>();
        project(events);
    }

    public void project(List<DomainEvent> events) {
        events.forEach(genericEvent -> {
            if (genericEvent instanceof SeatsWerePreparedForReservation) {
                availableSeats.addAll(
                    ((SeatsWerePreparedForReservation) genericEvent).getAvailableSeats()
                );
            }
            if (genericEvent instanceof SeatHasBeenReserved) {
                Seat reservedSeat = ((SeatHasBeenReserved) genericEvent).getReservedSeat();
                reservedSeats.add(reservedSeat);
                availableSeats.remove(reservedSeat);
            }
        });
    }

    public List<Seat> getReservedSeats() {
        return reservedSeats;
    }

    public List<Seat> getAvailableSeats() {
        return availableSeats;
    }
}
